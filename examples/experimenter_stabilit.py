import sys; sys.path.extend(['..'])
import warnings
import datetime

from fixout.core import FixOut
from runner import algo_parser, exp_parser
from examples.datasets_param import *


def one_experiment(source_name, sep, train_size, to_drop, all_categorical_features, max_features, algo, exp, sampling_size, threshold_exp):
    
    for i in range(0,100):   
        print(i,end=' ')
        fixout = FixOut(source_name, sep, train_size, to_drop, all_categorical_features, algo, exp, max_features, sampling_size, seed=i, threshold=threshold_exp)   
        _, is_fair, _, _ = fixout.is_fair()
        if not is_fair:
            print("UNFAIR")
        else:
            print("FAIR")
    
    
def main(general_param, data_param):
    one_experiment(data_param["source_name"], data_param["sep"], general_param["train_size"], data_param["sensitive"], data_param["all_categorical_features"], general_param["max_features"], algo_parser(general_param["algo"]), exp_parser(general_param["exp"]), general_param["sample_size"], general_param["threshold"])


general_parameters = {
    "train_size" : 0.7,
    "algo" : "rf",
    "exp" : "lime",
    "max_features" : 10,
    "sample_size" : 500,
    "threshold" : None
    }

if __name__ == "__main__":

    warnings.filterwarnings('ignore')
        
    dataset_param = german
    
    now = datetime.datetime.now()
    print(now.year,'-', now.month,'-', now.day,',', now.hour,':', now.minute,':', now.second,sep='')
    print("Data Parameters:",dataset_param)
    print("General Parameters:",general_parameters)
    main(general_parameters, dataset_param)
    