import sys; sys.path.extend(['..'])

import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
from fixout.core_text import EnsembleOutText
from fixout.lime_text_global import TextExplainer

from time import time

# loading data
categories = ['alt.atheism', 'soc.religion.christian']
newsgroups_train = fetch_20newsgroups(subset='train', categories=categories)
newsgroups_test = fetch_20newsgroups(subset='test', categories=categories)
X_train = newsgroups_train.data
y_train = newsgroups_train.target
X_test  = newsgroups_test.data
y_test  = newsgroups_test.target
class_names = ['atheism', 'christian']

# creating a model pipeline and training it
vectorizer = TfidfVectorizer(lowercase=True)
lr = LogisticRegression()
model = make_pipeline(vectorizer, lr)
model.fit(X_train, y_train)

# evaluating our model
print("Accuracy:", model.score(X_test, y_test))

# explaining our model
explainer = TextExplainer(model.predict_proba)
explainer.global_explanation(X_test, n_samples=250)

for word, contrib in explainer.get_top_k(k=10) :
    print(word, '\t', contrib)

# correcting fairness if necessary
sensitive_words = list(map(lambda x: [x], ["com", "nigel", "of", "host", "library", "canrem", "symposium", "desks", "edu"]))
ensemble = EnsembleOutText(model, sensitive_words)
ensemble.fit(X_train, y_train)
print("Ensemble accuracy:", ensemble.score(X_test, y_test))

# explaining the ensemble model
ensemble_explainer = TextExplainer(ensemble.predict_proba)
ensemble_explainer.global_explanation(X_test, n_samples=250)

for word, contrib in explainer.get_top_k(k=10) :
    print(word, '\t', contrib)
