import sys; sys.path.extend(['..'])

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk import pos_tag
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
# print("Training embeddings...")
# from examples.word_clustering import word_clustering_aa, word_clustering_sa
from examples.word_clustering import get_most_similar, embedding
from fairmodels import ModelProb, FairnessObject
# print("Ok let's go")
from copy import deepcopy
from time import time

from fixout.core_text import FixOutText, EnsembleOutText
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

hsdata = pd.read_csv("datasets/hate_speech_eg.csv").sample(frac=1)
class_names = np.array(["ok", "hate speech"])

stopwords = stopwords.words("english")
stopwords.extend(["#ff", "ff", "rt"])

stemmer = PorterStemmer()

def preprocess(text_string):
    """
    Accepts a text string and replaces:
    1) urls with URLHERE
    2) lots of whitespace with one instance
    3) mentions with MENTIONHERE

    This allows us to get standardized counts of urls and mentions
    Without caring about specific people mentioned
    """
    space_pattern = '\s+'
    giant_url_regex = ('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|'
        '[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    mention_regex = '@[\w\-]+'
    emoji_regex = '\\\\u[0-9a-fA-F]{4}'
    parsed_text = re.sub(space_pattern, ' ', text_string)
    parsed_text = re.sub(giant_url_regex, '', parsed_text)
    parsed_text = re.sub(mention_regex, '', parsed_text)
    parsed_text = re.sub(emoji_regex, '', parsed_text)
    parsed_text = parsed_text.replace("&amp;", "")
    return parsed_text


def tokenize(tweet):
    """Removes punctuation & excess whitespace, sets to lowercase,
    and stems tweets. Returns a list of stemmed tokens."""
    tweet = " ".join(re.split("[^a-zA-Z]*", tweet.lower())).strip()
    tokens = [stemmer.stem(t) for t in tweet.split()]
    return tokens


def basic_tokenize(tweet):
    """Same as tokenize but without the stemming"""
    tweet = " ".join(re.split("[^a-zA-Z.,!?]*", tweet.lower())).strip()
    return tweet.split()

tweet_tags = []
for tweet in hsdata.tweet:
    tokens = basic_tokenize(preprocess(tweet))
    tags = pos_tag(tokens)
    tag_list = [x[1] for x in tags]
    tag_str = " ".join(tag_list)
    tweet_tags.append(tag_str)



X = np.array(list(map(lambda text : " ".join(tokenize(preprocess(text))), hsdata.tweet.to_numpy())))
y = (hsdata["class"] != 2).to_numpy(dtype=np.uint8)
ev = hsdata["english_variant"]

X[ev == "SA"] = list(map(preprocess, X[ev == "SA"]))
X[ev == "AA"] = list(map(preprocess, X[ev == "AA"]))
X_train, X_test, y_train, y_test, ev_train, ev_test = train_test_split(X, y, ev, train_size=0.8)

vectorizer = TfidfVectorizer(
    tokenizer=tokenize,
    ngram_range=(1, 3),
    stop_words=stopwords,
    use_idf=True,
    smooth_idf=False,
    norm=None,
    decode_error='replace',
    max_features=10000,
    min_df=5,
    max_df=0.75
)

# vectors = vectorizer.fit_transform(X_train)
# vectors = vectors.toarray()
# pca = PCA(n_components=2)
# vectors2d = pca.fit_transform(vectors)
# plt.plot(vectors2d[:,0], vectors2d[:,1])
# plt.show()


lr = LogisticRegression(class_weight='balanced')
# rf = RandomForestClassifier(n_estimators=100)

# training the model
model = make_pipeline(vectorizer, lr)
# model_sa = deepcopy(model)
# model_aa = deepcopy(model)

model.fit(X_train, y_train)
# model_sa.fit(X_train[ev_train == "SA"], y_train[ev_train == "SA"])
# model_aa.fit(X_train[ev_train == "AA"], y_train[ev_train == "AA"])

# evaluating the model
pred = model.predict(X_test)
print("Accuracy:", accuracy_score(y_test, pred))
# print("SA Accuracy:", model_sa.score(X_test[ev_test == "SA"], y_test[ev_test == "SA"]))
# print("AA Accuracy:", model_aa.score(X_test[ev_test == "AA"], y_test[ev_test == "AA"]))

"""
# ~~~~~~~~~~~~~~~~~~~~~~~~~~ checking parity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_dataset_predictions(model_sa, model_aa, dataset_filename) :
    variant = pd.read_csv(dataset_filename)
    tweets = variant["tweet"].to_numpy()
    for i in range(tweets.shape[0]): tweets[i] = tweets[i][1:-1]
    tweets_sa = tweets[variant["group"] == "SA"]
    tweets_aa = tweets[variant["group"] == "AA"]
    return model_sa.predict(tweets_sa), model_aa.predict(tweets_aa)


def check_parity(preds_sa, preds_aa, word=None):
    hate_rate_sa = len(preds_sa[preds_sa == 1]) / len(preds_sa)
    hate_rate_aa = len(preds_aa[preds_aa == 1]) / len(preds_aa)
    print(f"[{word}] P(hate_speech | SA) = {hate_rate_sa}")
    print(f"[{word}] P(hate_speech | AA) = {hate_rate_aa}")
    return hate_rate_sa, hate_rate_aa

print("Original model")
preds_sa, preds_aa = get_dataset_predictions(model, model, "datasets/english_variant.csv")
check_parity(preds_sa, preds_aa, "")
preds_sa, preds_aa = get_dataset_predictions(model, model, "datasets/english_variant_btch.csv")
check_parity(preds_sa, preds_aa, "b*tch")
preds_sa, preds_aa = get_dataset_predictions(model, model, "datasets/english_variant_ngga.csv")
check_parity(preds_sa, preds_aa, "n*gga")
"""
# # difference per word
# sensitive_words = ['pandora', 'glad', 'niggah', 'half', 'yu', 'year', 'ude14', 'b', 'night', 'niggas', 'why', 'motherfuckers', 'white', 'go', 'stfu', 'right', 'asap', 'name', 'fairy', 'yall', 'listen', 'any', 'side', 'gon', 'niggers', 'nigger', 'emotions', 'continue', 'shit', 'rated', 'via', 'im', 'wen', 'fucked', 'friends', 'afraid', 'pussy', 'take', 'let', 'smh', 'dope', 'life', 'fb', 'booty', 'oomf', 'yankees', 'queer', 'sex', 'damn', 'fuckin', 'fck', 'amp', 'think', 'hoe', 'need', 'never', 'lmao', 'hoes', 'tl', 'dnt', 'sorry', 'yeah', 'love', 'fa', 'wtf', 'big', 'like', 'udc4c', 'wanna', 'gone', 'ud83d', 'dont', 'fucking', 'know', 'bird', 'af', 'thug', 'nicca', 'god', 'loyal', 'fucks', 'dis', 'little', 'n', 'retarded', 'automatically', 'totally', 'ipad', 'ctfu', 'females', 'visit', 'call', 'faggot', 'happy', 'one', 'everyone', 'hahaha', 'burger', 'slut', 'american', 'thank', 'wet', 'pussies', 'king', 'ud83c', 'lol', 'cunt', 'game', 'wow', 'niccas', 'bout', 'haha', 'ass', 'hate', 'yellow', 'iphone', 'bitch', 'niggahs', 'good', 'got', 'class', 'list', 'home', 'nigs', 'da', 'bitches', 'told', 'business', 'could', 'stop', 'twitter', 'park', 'man', 'tell', 'fuck', 'nobody', 'make', 'ya', 'yo', 'lil', 'faggots', 'gay', 'se', 'coffee', 'best', 'kill', 'dick', 'nig', 'job', 'though', 'birds', 'tho', 'dyke', 'thanks', 'yea', 'retard', 'alright', 'found', 'android', 'cc', 'much', 'nigga', 'ude0d', 'u', 'gt', 'back', 'fags', 'wit', 'em', 'honkies', 'imma', 'gotta', 'above', 'cause', 'slope', 'stupid', 'book', 'fag']
# # words in the top-100 lime global for the hate speech model, and for the english variant classifier
# results = []
# for w in sensitive_words :
#     print(w)
#     score_dict = {"word": w}
#     ensemble = EnsembleOutText(model, [w])
#     ensemble.fit(X_train, y_train)
#     score_dict["accuracy"] = ensemble.score(X_test, y_test)
#     preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant.csv")
#     hate_rate_sa, hate_rate_aa = check_parity(preds_sa, preds_aa, "")
#     score_dict["hate_speech | SA"] = hate_rate_sa
#     score_dict["hate_speech | AA"] = hate_rate_aa
#     score_dict["hate_speech (difference)"] = abs(hate_rate_sa - hate_rate_aa)
#     preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant_btch.csv")
#     hate_rate_sa, hate_rate_aa = check_parity(preds_sa, preds_aa, "b*tch")
#     score_dict["[b*tch] hate_speech | SA"] = hate_rate_sa
#     score_dict["[b*tch] hate_speech | AA"] = hate_rate_aa
#     score_dict["[b*tch] hate_speech (difference)"] = abs(hate_rate_sa - hate_rate_aa)
#     preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant_ngga.csv")
#     hate_rate_sa, hate_rate_aa = check_parity(preds_sa, preds_aa, "n*gga")
#     score_dict["[n*gga] hate_speech | SA"] = hate_rate_sa
#     score_dict["[n*gga] hate_speech | AA"] = hate_rate_aa
#     score_dict["[n*gga] hate_speech (difference)"] = abs(hate_rate_sa - hate_rate_aa)
#     results.append(score_dict)
#
# results_df = pd.DataFrame(results)

# print("Contextual model")
# preds_sa, preds_aa = get_dataset_predictions(model_sa, model_aa, "datasets/english_variant.csv")
# check_parity(preds_sa, preds_aa, None)
# preds_sa, preds_aa = get_dataset_predictions(model_sa, model_aa, "datasets/english_variant_btch.csv")
# check_parity(preds_sa, preds_aa, "b*tch")
# preds_sa, preds_aa = get_dataset_predictions(model_sa, model_aa, "datasets/english_variant_ngga.csv")
# check_parity(preds_sa, preds_aa, "n*gga")

# explaining the model
# fixout = FixOutText(X, y, sensitives=["black", "white", "bitch"], max_features=-1)
# t0 = time()
# fair_flag, words_weights, actual_sensitive, explainer = fixout.is_fair(model)
# words_weights.to_csv("/home/fabien/Documents/Orpa/explanations/lime_mean.csv", index=False)
# print("took", time()-t0, "seconds")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~ Applying EnsembleOut ~~~~~~~~~~~~~~~~~~~~~~~~~
# top_words = ["lol", "like", "shit", "tho", "got", "nigga"] #, "dont", "ass", "gotta", "much", "end", "lmao", "haha", "thank", "call", "bout", "ctfu", "also", "littl", "finna"]
# top_words = [tokenize(preprocess(w))[0] for w in top_words]
# sensitive_groups = [get_most_similar(w, embedding, min_similarity=0.9) for w in top_words]
sensitive_groups = [["nigga", "niggah", "niggas", "niggahs"], ["nigger", "niggers"], ["nig", "nigs"], ["nicca", "niccas"]]
print(sensitive_groups)
ensemble = EnsembleOutText(model, sensitive_groups)
ensemble.fit(X_train, y_train)
ens_pred = ensemble.predict(X_test)
print("Ensemble accuracy:", accuracy_score(y_test, ens_pred))

fixout = FixOutText(X_train, y_train, sensitives=sensitive_groups, max_features=-1)
fair_flag_ens, words_weights_ens, actual_sensitive_ens, explainer_ens = fixout.is_fair(ensemble)

"""
preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant.csv")
check_parity(preds_sa, preds_aa, "")
preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant_btch.csv")
check_parity(preds_sa, preds_aa, "b*tch")
preds_sa, preds_aa = get_dataset_predictions(ensemble, ensemble, "datasets/english_variant_ngga.csv")
check_parity(preds_sa, preds_aa, "n*gga")
"""

# df = pd.read_csv("datasets/english_variant.csv")
# pred = model.predict_proba(df["tweet"])[:,1]
# ens_pred = ensemble.predict_proba(df["tweet"])[:,1]
# nigga_group = sensitive_groups[-1]
# contains_nigga = np.array(["none", "nigga"])[[min(sum(int(word in tweet) for word in nigga_group), 1) for tweet in X_test]]
# contains_nigga = np.array(["none", "nigga"])[[int("nigga" in tweet) for tweet in X_test]]
#
# lm = ModelProb(preds=pred, threshold=0.5, name="Original model")
# ens = ModelProb(preds=ens_pred, threshold=0.5, name="EnsembleOut")

# fobject = FairnessObject(
#     model_probs=[lm, ens],
#     y=y_test,
#     protected=contains_nigga,
#     privileged="none"
# )
# plt = fobject.plot(fairness_metrics=['acc', 'tpr', "tnr", 'ppv', "npv", 'fpr', "fnr", "stp", "fdr", "for", "ts", "f1"])
# plt.show()
