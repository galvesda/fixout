import sys; sys.path.extend(['..'])

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk import pos_tag
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from random import randint, choice

from fixout.core_text import FixOutText, EnsembleOutText
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

hsdata = pd.read_csv("datasets/hate_speech.csv").sample(frac=1)
engroup = pd.read_csv("datasets/english_variant.csv")
class_names = np.array(["ok", "hate speech"])

stopwords = stopwords.words("english")
stopwords.extend(["#ff", "ff", "rt"])

stemmer = PorterStemmer()

def preprocess(text_string):
    """
    Accepts a text string and replaces:
    1) urls with URLHERE
    2) lots of whitespace with one instance
    3) mentions with MENTIONHERE

    This allows us to get standardized counts of urls and mentions
    Without caring about specific people mentioned
    """
    space_pattern = '\s+'
    giant_url_regex = ('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|'
        '[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    mention_regex = '@[\w\-]+'
    emoji_regex = '\\\\u[0-9a-fA-F]{4}'
    parsed_text = re.sub(space_pattern, ' ', text_string)
    parsed_text = re.sub(giant_url_regex, '', parsed_text)
    parsed_text = re.sub(mention_regex, '', parsed_text)
    parsed_text = re.sub(emoji_regex, '', parsed_text)
    parsed_text = parsed_text.replace("&amp;", "")
    return parsed_text


def tokenize(tweet):
    """Removes punctuation & excess whitespace, sets to lowercase,
    and stems tweets. Returns a list of stemmed tokens."""
    tweet = " ".join(re.split("[^a-zA-Z]*", tweet.lower())).strip()
    tokens = [stemmer.stem(t) for t in tweet.split()]
    return tokens


def basic_tokenize(tweet):
    """Same as tokenize but without the stemming"""
    tweet = " ".join(re.split("[^a-zA-Z.,!?]*", tweet.lower())).strip()
    return tweet.split()

tweet_tags = []
for tweet in hsdata.tweet:
    tokens = basic_tokenize(preprocess(tweet))
    tags = pos_tag(tokens)
    tag_list = [x[1] for x in tags]
    tag_str = " ".join(tag_list)
    tweet_tags.append(tag_str)

vectorizer = TfidfVectorizer(
    tokenizer=tokenize,
    ngram_range=(1, 3),
    stop_words=stopwords,
    preprocessor=preprocess,
    use_idf=True,
    smooth_idf=False,
    norm=None,
    decode_error='replace',
    max_features=10000,
    min_df=5,
    max_df=0.75
)

base_words = """nigga
bitch
""".split('\n')

init_groups = [['lol', 'damn', 'lmao', 'actin', 'swerv', 'tho', 'naw', 'yassss', 'smh', 'dumb'],
    ['like', 'call', 'good'],
    ['shit', 'nigga', 'nicca', 'real', 'money', 'niggah', 'nothin', 'bout', 'noth', 'hoe'],
    ['tho', 'fuc', 'neva', 'naw', 'trippin', 'problem', 'gtfoh', 'eater', 'gone', 'ju'],
    ['got', 'love', 'had', 'wit', 'swear', 'aint', 'niggga', 'give', 'told', 'gotta'],
    ['nigga', 'shit', 'real', 'iffi', 'aint', 'money', 'niggah', 'wit', 'frontin', 'broke'],
    ['dont', 'wanna', 'mad', 'caus', 'realli', 'care', 'trust', 'worri', 'even', 'never'],
    ['ass', 'anonym', 'shut', 'yo', 'wit', 'up', 'dick', 'fuck', 'lil', 'dumb'],
    ['gotta', 'cuz', 'owe', 'lie', 'gone', 'gon', 'pow', 'wanna', 'knw', 'believ'],
    ['much', 'gonna', 'mani', 'someth', 'wonder', 'funni', 'brag', 'everyth', 'arrog', 'mayb']]

X = hsdata["tweet"]
y = (hsdata["class"] != 2).to_numpy(dtype=np.uint8)

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8)

sa_tweets = engroup["tweet"][engroup["group"] == "SA"]
aa_tweets = engroup["tweet"][engroup["group"] == "AA"]

vectorizer.fit(X_train)
lr = LogisticRegression(class_weight='balanced')


def error(model) :
    sa_preds = model.predict(sa_tweets)
    aa_preds = model.predict(aa_tweets)
    sa_count = len(sa_preds[sa_preds == 1]) / sa_preds.shape[0]
    aa_count = len(aa_preds[aa_preds == 1]) / aa_preds.shape[0]
    return abs(sa_count - aa_count)


def merge(model1, model2) :
    groups1 = model1.groups
    groups2 = model2.groups
    assert len(groups1) == len(groups1), "Models should have the same number of groups"

    new_groups = []
    for g1, g2 in zip(groups1, groups2) :
        if randint(0, 1) :
            new_groups.append(g1)
        else :
            new_groups.append(g2)

    return new_groups


def mutation(groups) :
    new_groups = []
    for g in groups :
        ng = []
        for w in g :
            if randint(0, 9) > 0 :
                ng.append(w)
            if randint(0, 9) == 0 :
                ng.append(choice(base_words))
        new_groups.append(ng)
    return EnsembleOutText(lr, new_groups, tokenizer=vectorizer.transform)


population = [mutation(init_groups) for _ in range(50)]
for epoch in range(50) :
    print(f"Epoch {epoch}")
    for model in population :
        model.fit(X_train, y_train)
    rank = sorted(population, key=lambda m: error(m))
    parent1 = rank[0]
    parent2 = rank[1]
    print("Error:", error(parent1))
    print("Accuracy:", parent1.score(X_test, y_test))

    population = [mutation(merge(parent1, parent2)) for _ in range(50)]