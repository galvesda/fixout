from gensim.models import Word2Vec
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans, DBSCAN

import nltk
from nltk.stem.porter import *
from nltk import pos_tag

from copy import deepcopy

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')

hsdata = pd.read_csv("datasets/hate_speech.csv")
# hsdata = pd.read_csv("datasets/english_variant_100k.csv")
class_names = np.array(["ok", "hate speech"])

stopwords = nltk.corpus.stopwords.words("english")
stopwords.extend(["#ff", "ff", "rt"])

stemmer = PorterStemmer()


def preprocess(text_string):
    """
    Accepts a text string and replaces:
    1) urls with URLHERE
    2) lots of whitespace with one instance
    3) mentions with MENTIONHERE

    This allows us to get standardized counts of urls and mentions
    Without caring about specific people mentioned
    """
    space_pattern = '\s+'
    giant_url_regex = ('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|'
                       '[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    mention_regex = '@[\w\-]+'
    emoji_regex = '\\\\u[0-9a-fA-F]{4}'
    parsed_text = re.sub(space_pattern, ' ', text_string)
    parsed_text = re.sub(giant_url_regex, '', parsed_text)
    parsed_text = re.sub(mention_regex, '', parsed_text)
    parsed_text = re.sub(emoji_regex, '', parsed_text)
    parsed_text = parsed_text.replace("&amp;", "")
    return parsed_text


def tokenize(tweet):
    """Removes punctuation & excess whitespace, sets to lowercase,
    and stems tweets. Returns a list of stemmed tokens."""
    tweet = " ".join(re.split("[^a-zA-Z]*", tweet.lower())).strip()
    tokens = [stemmer.stem(t) for t in tweet.split()]
    return tokens


def basic_tokenize(tweet):
    """Same as tokenize but without the stemming"""
    tweet = " ".join(re.split("[^a-zA-Z.,!?]*", tweet.lower())).strip()
    return tweet.split()


tweet_tags = []
for tweet in hsdata.tweet:
    tokens = basic_tokenize(preprocess(tweet))
    tags = pos_tag(tokens)
    tag_list = [x[1] for x in tags]
    tag_str = " ".join(tag_list)
    tweet_tags.append(tag_str)


tweets = list(map(lambda s: tokenize(preprocess(s)), hsdata.tweet))
# tweets_aa = list(map(lambda s: tokenize(preprocess(s)), hsdata.tweet[hsdata["group"] == "AA"]))
# tweets_sa = list(map(lambda s: tokenize(preprocess(s)), hsdata.tweet[hsdata["group"] == "SA"]))
embedding = Word2Vec(sentences=tweets)
# embedding_aa = Word2Vec(sentences=tweets_aa)
# embedding_sa = Word2Vec(sentences=tweets_sa)


def get_vocab(embedding):
    vectors = []
    vocab = []
    for w in embedding.wv.vocab:
        vocab.append(w)
        vectors.append(embedding[w])
    vectors = np.array(vectors)
    vocab = np.array(vocab)
    return vocab, vectors


def get_clustering_function(embedding):
    vocab, vectors = get_vocab(embedding)
    cl = DBSCAN(eps=0.2) # dbscan nul!
    cl.fit(vectors)
    cl.clusters_ids = set(cl.labels_)
    cl.n_clusters = len(cl.clusters_ids)

    mapping = []
    for i in cl.clusters_ids:
        center = vectors[cl.labels_ == i][0]
        word = embedding.wv.similar_by_vector(center)[0][0]
        mapping.append((set(vocab[cl.labels_ == i]), word))

    def word_to_cluster(word):
        for s, w in mapping:
            if word in s:
                return w
        return word

    def word_clustering(string):
        tok = tokenize(preprocess(string))
        return " ".join(map(word_to_cluster, tok))

    return word_clustering, cl, vocab, vectors


def get_most_similar(word, embedding, min_similarity=0.85, max_words=50) :
    return [word] + list(
        map(
            lambda x: x[0],
            filter(
                lambda x: x[1] >= min_similarity,
                embedding.wv.most_similar_cosmul([word], topn=max_words)
            )
        )
    )


# word_clustering_aa, kmeans_aa, vocab_aa, vectors_aa = get_clustering_function(embedding_aa)
# word_clustering_sa, kmeans_sa, vocab_sa, vectors_sa = get_clustering_function(embedding_sa)
# word_clustering_aa("You are very happy today ! ;)")
#
# print([len(kmeans_aa.labels_[kmeans_aa.labels_ == i]) for i in range(40)])
# print([len(kmeans_sa.labels_[kmeans_sa.labels_ == i]) for i in range(40)])
#
# len(kmeans_sa.cluster_centers_)
