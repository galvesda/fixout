import sys; sys.path.extend(['..'])

import pandas as pd
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

from fixout.lime_tabular_global import TabularExplainer
from fixout.core_tabular import EnsembleOutTabular
from fixout.utils import columns_preprocessers, transform_categorical_names

# load the data and convert it to numpy
data = pd.read_csv("/home/fabien/Documents/Orpa/fixout-pkdd/datasets/adult2.data")
features = data.columns[:-1]
X = data.drop(columns="Target").to_numpy()
y = data["Target"].to_numpy()

# preprocess the data with respect to their type (categorical/numerical)
categorical = [1, 3, 5, 6, 7, 8, 9, 13]
categories = transform_categorical_names(X, categorical, feature_names=features)
ct = columns_preprocessers(X, categorical,
                           categorical_preprocesser=OneHotEncoder())

# split the data and train a model
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

lr = LogisticRegression()
model = make_pipeline(ct, lr)
model.fit(X_train, y_train)
print("Original score:", model.score(X_test, y_test))

# explain the original model
explainer_original = TabularExplainer(model.predict_proba, X_train, categorical_features=categorical)
explainer_original.global_explanation(n_samples=200)

for i, contrib in explainer_original.get_top_k(k=10) :
    print(features[i], '\t', contrib)

# make an ensemble
ensemble = EnsembleOutTabular(lr, ct, sensitive_features=(5, 8, 9))
ensemble.fit(X_train, y_train)
print("Ensemble score:", ensemble.score(X_test, y_test))

# explain the ensemble
explainer_ensemble = TabularExplainer(ensemble.predict_proba, X_train, categorical_features=categorical)
explainer_ensemble.global_explanation(n_samples=200)

for i, contrib in explainer_ensemble.get_top_k(k=10) :
    print(features[i], '\t', contrib)
