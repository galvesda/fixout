"""
Implements LIME_Global. Verifies if sensitives features have high contributions.
"""
import numpy as np
from fixout.lime.lime_tabular import LimeTabularExplainer

class TabularExplainer:
    def __init__(self, predict_fn, X_data, **kwargs):
        self.predict_fn = predict_fn
        self.X_data = X_data
        self.explainer = LimeTabularExplainer(X_data, **kwargs)
        self.explanations = None

    def explain_instances(self, X):
        explanations = []
        for instance in X:
            exp = self.explainer.explain_instance(
                    instance,
                    self.predict_fn,
                    num_features=len(instance)
                ).local_exp[1]
            local_exp = np.zeros((1, len(exp)))
            for i, v in exp:
                local_exp[0,i] = v
            explanations.append(local_exp)
        return np.concatenate(explanations)

    def global_explanation(self, X_data=None, n_samples=5000):
        if X_data is None:
            X_data = self.X_data
        I = np.arange(len(X_data))
        np.random.shuffle(I)
        I = I[:n_samples]
        X = X_data[I]
        explanations = self.explain_instances(X)
        self.explanations = np.mean(explanations, axis=0)
        return self

    def get_top_k(self, k=10):
        if self.explanations is None:
            return
        top = sorted(enumerate(self.explanations), key=lambda x: abs(x[1]), reverse=True)[:k]
        return top