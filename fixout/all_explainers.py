 
from collections import Counter

from fixout.anchor import anchor_tabular
from fixout import lime_global
import pandas as pd
import numpy as np


def fairness_eval(model, train, max_features, sensitive_features, feature_names, class_names, categorical_features, categorical_names):
    
    _, sp_obj = lime_global.features_contributions(model.predict_proba, train, feature_names, max_features, class_names, categorical_features, categorical_names)
    
    a = Counter()
    for i in sp_obj.V:
        exp = sp_obj.explanations[i]
        a1 = Counter(dict(exp.local_exp[1]))
        a.update(a1)
    
    ans_data = []
    for key in a:
        ans_data1 = [feature_names[key],a[key]]
        ans_data.append(ans_data1)
        
    df = pd.DataFrame(ans_data, columns = ["Feature", "Contribution"])
    print(df.iloc[(-np.abs(df['Contribution'].values)).argsort()])
    
    
    indices = sp_obj.indices
    a_explainer = anchor_tabular.AnchorTabularExplainer(class_names, feature_names, train, categorical_names=categorical_names)
    
    non_empty_anchors = 0
    counter = Counter()
    for i in indices:
        exp = a_explainer.explain_instance(train[i], model.predict, threshold=0.95)
        print(i,'%.2f' % exp.precision(),' %.2f' % exp.coverage(), ' (class %s)' % exp.exp_map['prediction'], '%s' % (' AND '.join(exp.names())))
        features = exp.exp_map['feature']
        if len(features) > 0:
            a1 = Counter(features)
            non_empty_anchors += 1
            counter.update(a1)
    
    ans_data = []
    for key, value in sorted(counter.items(), key=lambda x: x[1], reverse=True):
        ans_data1 = [feature_names[key],value/non_empty_anchors]
        ans_data.append(ans_data1)
        
    df = pd.DataFrame(ans_data, columns = ["Feature", "Frequency"])
    print(df.iloc[(-np.abs(df['Frequency'].values)).argsort()])
    
    
