from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import LabelEncoder

def columns_preprocessers(X, categorical_indexes, categorical_preprocesser, numerical_preprocesser=None, remainder="passthrough"):
    ct = ColumnTransformer([("ohe", categorical_preprocesser, categorical_indexes)], remainder=remainder)
    ct.fit(X)
    if numerical_preprocesser is not None:
        numerical = list(set(range(len(X[0]))) - set(categorical_indexes))
        scaler = ColumnTransformer([("scaler", numerical_preprocesser, numerical)], remainder=remainder)
        scaler.fit(X)
        return ct, scaler
    return ct

def transform_categorical_names(X, categorical_indexes, feature_names=None):
    le = LabelEncoder()
    names = {}
    for i in categorical_indexes:
        X[:, i] = le.fit_transform(X[:, i])
        if feature_names is None :
            k = i
        else :
            k = feature_names[i]
        names[k] = le.classes_
    return names
