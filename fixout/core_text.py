"""
Implements the main procedures to build fairer ensembles, e.g. feature drop out, model training, ensemble bulding
"""
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from sklearn.metrics import accuracy_score
from sklearn.base import ClassifierMixin
from copy import deepcopy

import numpy as np
import re


class EnsembleOutText(ClassifierMixin):
    """Class for ensemble models
    
    Saves a list of trained classifiers and their respective encoders and deleted features

    Args:
        models: a list of trained classifiers
    """
    def __init__(self,
                 base_model,
                 sensitive_groups=(),
                 tokenizer=None,
                 ignorecase=True,
                 token_pattern=r"\b\w+\b",
                 auto_threshold=False):
        self.models = [base_model]
        self.tokenizer = tokenizer
        self.groups = sensitive_groups
        self.removers = []
        self.threshold = 0.5
        self.dtype = np.uint8
        self.auto_threshold = auto_threshold

        flags = re.IGNORECASE if ignorecase else 0
        all_words = []

        for group in sensitive_groups:                                                  # getting the group of words to drop
            all_words.extend(group)
            rep = re.compile(token_pattern.replace("\\w", '('+ '|'.join(group) +')'), flags=flags)   # compiling a regex to find the word
            self.removers.append(rep)

        rep_all = re.compile(token_pattern.replace("\\w", '(' + '|'.join(all_words) + ')'), flags=flags)
        self.removers.append(rep_all)

    def fit(self, X, y):
        try:
            self.dtype = y.dtype
        except:
            pass
        base_model = self.models.pop(0)

        for rep in self.removers:
            X_train = list(map(lambda text: rep.sub("", text), X))
            model = deepcopy(base_model)
            if self.tokenizer is not None :
                X_train = self.tokenizer(X_train)
            model.fit(X_train, y)
            self.models.append(model)

        if self.auto_threshold:
            self.adjust_threshold(X, y)

    def adjust_threshold(self, X, y):
        thresholds = np.arange(0, 1, 0.001)
        scores = [accuracy_score(y, self.predict(X, threshold=t)) for t in thresholds]  # score each threshold
        i = np.argmax(scores)
        self.threshold = thresholds[i]

    def predict_proba(self, X):
        """
        Returns probability for each class label.
        """
        probs = []
        n_models = len(self.models)
        
        for i in range(n_models):
            model = self.models[i]
            rep = self.removers[i]
            X_ = X
            if rep is not None :
                X_ = list(map(lambda s: rep.sub("", s), X))
            if self.tokenizer is not None :
                X_ = self.tokenizer(X_)
            y = model.predict_proba(X_)
            probs.append(y)

        return np.array(probs).mean(axis=0)

    def predict(self, X, threshold=None):
        if threshold is None: threshold = self.threshold
        return (self.predict_proba(X)[:,1] > threshold).astype(self.dtype)