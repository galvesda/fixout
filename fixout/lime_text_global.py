"""
Implements LIME_Global. Verifies if sensitives features have high contributions.
"""
import numpy as np
from fixout.lime.lime_text import LimeTextExplainer
from collections import Counter


class TextExplainer:
    def __init__(self, predict_fn, X_data=None, **kwargs):
        self.predict_fn = predict_fn
        self.X_data = X_data
        self.explainer = LimeTextExplainer(**kwargs)
        self.explanations = None

    def explain_instances(self, X):
        explanations = []
        for instance in X:
            exp = self.explainer.explain_instance(
                    instance.lower(),
                    self.predict_fn
                )
            vocab = exp.domain_mapper.indexed_string.inverse_vocab
            local_exp = {vocab[i]: weight for i, weight in exp.local_exp[1]}
            explanations.append(local_exp)
        return explanations

    def global_explanation(self, X_data=None, n_samples=5000):
        if X_data is None:
            X_data = self.X_data
        I = np.arange(len(X_data))
        np.random.shuffle(I)
        I = I[:n_samples]
        X = np.array(X_data)[I]
        explanations = self.explain_instances(X)
        contributions = Counter()
        tuple(map(contributions.update, explanations))
        self.explanations = contributions
        return self

    def get_top_k(self, k=10):
        if self.explanations is None:
            return
        words = self.explanations.keys()
        values = self.explanations.values()
        top = sorted(zip(words, values), key=lambda x: abs(x[1]), reverse=True)[:k]
        return top
