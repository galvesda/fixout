"""
Implements the main procedures to build fairer ensembles, e.g. feature drop out, model training, ensemble bulding
"""
import os

from sklearn.pipeline import make_pipeline

os.environ['KMP_DUPLICATE_LIB_OK']='True'

from sklearn.metrics import accuracy_score
from sklearn.base import ClassifierMixin
from fixout.lime_tabular_global import TabularExplainer
from copy import deepcopy

import numpy as np

class EnsembleOutTabular(ClassifierMixin):
    """Class for ensemble models
    
    Saves a list of trained classifiers and their respective encoders and deleted features

    Args:
        models: a list of trained classifiers
    """
    def __init__(self,
                 base_model,
                 column_transformer=None,
                 sensitive_features=(),
                 auto_threshold=False):
        self.models = [base_model]
        self.column_transformer = column_transformer
        self.sensitives = [[i] for i in sensitive_features] \
                          + ([list(sensitive_features)] if len(sensitive_features) > 1 else [])
        self.threshold = 0.5
        self.dtype = np.uint8
        self.classes_ = np.array([False, True])
        self.auto_threshold = auto_threshold

    def fit(self, X, y):
        try:
            self.dtype = y.dtype
        except:
            pass
        base_model = self.models.pop(0)
        base_ct = self.column_transformer

        for features in self.sensitives:
            # feature drop
            X_train = np.array(X)
            for i in sorted(features, reverse=True):
                X_train = np.delete(X_train, i, axis=1)
            # categorical preprocessing
            if base_ct is not None:
                ct = deepcopy(base_ct)
                ct.transformers = self._drop_transformers_columns(ct.transformers, features)
                ct.set_params()
                model = make_pipeline(ct, deepcopy(base_model))
            else:
                model = deepcopy(base_model)
            model.fit(X_train, y)
            self.models.append(model)

        self.classes_ = self.models[-1].classes_

        if self.auto_threshold:
            self._adjust_threshold(X, y)

    def _adjust_threshold(self, X, y):
        thresholds = np.arange(0, 1, 0.001)
        scores = [accuracy_score(y, self.predict(X, threshold=t)) for t in thresholds]  # score each threshold
        i = np.argmax(scores)
        self.threshold = thresholds[i]

    def predict_proba(self, X):
        """
        Returns probability for each class label.
        """
        probs = []
        n_models = len(self.models)
        
        for i in range(n_models):
            model = self.models[i]
            features = self.sensitives[i]
            X_ = np.array(X)
            for i in sorted(features, reverse=True):
                X_ = np.delete(X_, i, axis=1)
            y = model.predict_proba(X_)
            probs.append(y)

        return np.array(probs).mean(axis=0)

    def predict(self, X, threshold=None):
        if threshold is None: threshold = self.threshold
        return self.classes_[(self.predict_proba(X)[:,1] > threshold).astype(np.uint8)]

    def _column_drop(self, columns, i):
        new_columns = []
        for j in columns:
            if j < i:
                new_columns.append(j)
            elif j > i:
                new_columns.append(j-1)
        return new_columns

    def _column_drop_many(self, columns, indexes):
        for i in sorted(indexes, reverse=True):
            columns = self._column_drop(columns, i)
        return columns

    def _drop_transformers_columns(self, transformers, indexes):
        new_transformers = []
        for name, preprocesser, columns in transformers:
            columns = self._column_drop_many(columns, indexes)
            new_transformers.append((name, preprocesser, columns))
        return new_transformers