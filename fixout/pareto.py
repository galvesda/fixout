import numpy as np
import matplotlib.pyplot as plt

import oapackage

datapoints = np.array([[2.8, 14.1, 6],
[9.95, 13.1, 5],
[14.8, 8.65, 4],
[16.9, 2.2, 3],
[5.55, 10.3, 2],
[8.6, 6.75, 1],
[11.15, 3.3, 0]])

# datapoints = np.array([[3.25, 10.65],
# [2.4, 4.85],
# [5.85, 1.6],
# [11.2, 1.35],
# [2.15, 14.7],
# [15, 11.6],
# [20.5, 1.4]])


# for i in range(0, datapoints.shape[1]):
#     w=datapoints[:,i]
#     fac=.6+.4*np.linalg.norm(w)
#     datapoints[:,i]=(1/fac)*w


pareto=oapackage.ParetoDoubleLong()

for i in range(0, datapoints.shape[0]):
    print(datapoints[i,0], datapoints[i,1])
    w=oapackage.doubleVector( (datapoints[i,0], datapoints[i,1]))
    pareto.addvalue(w, i)

pareto.show(verbose=1)

lst=pareto.allindices() # the indices of the Pareto optimal designs

optimal_datapoints=datapoints[lst,:]

print(optimal_datapoints)
 
h=plt.plot(datapoints[:,0], datapoints[:,1], '.b', markersize=16, label='Non Pareto-optimal')
hp=plt.plot(optimal_datapoints[:,0], optimal_datapoints[:,1], '.r', markersize=16, label='Pareto optimal')
plt.xlabel('Objective 1', fontsize=16)
plt.ylabel('Objective 2', fontsize=16)
plt.xticks([])
plt.yticks([])
_=plt.legend(loc=3, numpoints=1)
 
plt.show()
