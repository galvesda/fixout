 
from collections import Counter
import math
import random

import oapackage

from fixout.anchor import anchor_tabular
from fixout import lime_global
import numpy as np
import pandas as pd


def fairness_eval(model, train, max_features, sensitive_features, feature_names, class_names, categorical_features, categorical_names, sample_size):
    
    _, sp_obj = lime_global.features_contributions(model.predict_proba, train, feature_names, max_features, class_names, categorical_features, categorical_names, sample_size)
    
    indices = sp_obj.indices
    a_explainers = anchor_tabular.AnchorTabularExplainer(class_names, feature_names, train, categorical_names=categorical_names)
    
    non_empty_anchors = 0
    counter = Counter()
    for i in indices:
        exp = a_explainers.explain_instance(train[i], model.predict, threshold=0.95)
        print(i,'%.2f' % exp.precision(),' %.2f' % exp.coverage(), ' (class %s)' % exp.exp_map['prediction'], '%s' % (' AND '.join(exp.names())))
        features = exp.exp_map['feature']
        if len(features) > 0:
            a1 = Counter(features)
            non_empty_anchors += 1
            counter.update(a1)
    
    
    is_fair = True
    i = 0
    ans_data = []
    for key, value in sorted(counter.items(), key=lambda x: x[1], reverse=True):
        ans_data1 = [feature_names[key],value/non_empty_anchors]
        ans_data.append(ans_data1)
        if i < max_features and key in sensitive_features:
            is_fair = False
        i += 1
        
    df = pd.DataFrame(ans_data, columns = ["Feature", "Frequency"])
    print(df.iloc[(-np.abs(df['Frequency'].values)).argsort()])
    
    
    return is_fair, ans_data, a_explainers    

    

def fairness_eval2(model, train, max_features, sensitive_features, feature_names, class_names, categorical_features, categorical_names, sample_size):
    
#     _, sp_obj = lime_global.features_contributions(model.prob, train, feature_names, max_features, class_names, categorical_features, categorical_names, sample_size)
    
#     indices = sp_obj.indices
    indices = random.choices(range(len(train)), k=sample_size)
#     indices = list(range(800))
    a_explainers = anchor_tabular.AnchorTabularExplainer(class_names, feature_names, train, categorical_names=categorical_names)
    
    anchors_pos = []
    anchors_neg = []
    counter = 0
    for i in indices:
        anchor = a_explainers.explain_instance(train[i], model.predict, threshold=0.95)
        print(counter, i,'%.2f' % anchor.precision(),' %.2f' % anchor.coverage(), ' (class %s)' % anchor.exp_map['prediction'], '%s' % (' AND '.join(anchor.names())))
        
        label = anchor.exp_map['prediction']
        
        if label == 0:
            anchors_pos.append(anchor)
        elif label == 1:
            anchors_neg.append(anchor)
        counter += 1
        
    
    distinct_anchors_set = distinct_anchors(non_empty_anchors_fn(anchors_pos))
    optimal_pos_anchors = multi_pareto_set(distinct_anchors_set)
    pos_scores = scores(optimal_pos_anchors)
    
    distinct_anchors_set = distinct_anchors(non_empty_anchors_fn(anchors_neg))
    optimal_neg_anchors = multi_pareto_set(distinct_anchors_set)
    neg_scores = scores(optimal_neg_anchors)
    
    print("pareto size pos:",len(optimal_pos_anchors))
    print("pareto size neg:",len(optimal_neg_anchors))
    
    features_contributions = {}
    for feature_index, contribution in pos_scores.items():
        if feature_index in neg_scores:
            features_contributions[feature_index] = contribution + neg_scores[feature_index]
        else:
            features_contributions[feature_index] = contribution
    
    for feature_index, contribution in neg_scores.items():
        if feature_index not in features_contributions:
            features_contributions[feature_index] = contribution
            
    is_fair = True
#     i = 0
    ans_data = []
    for key, value in sorted(features_contributions.items(), key=lambda x: math.fabs(x[1]), reverse=True):
        ans_data1 = [feature_names[key],value]
        ans_data.append(ans_data1)
#         if i < max_features and key in sensitive_features:
#             is_fair = False
#         i += 1
        
    df = pd.DataFrame(ans_data, columns = ["Feature", "Frequency"])
    print(df.iloc[(-np.abs(df['Frequency'].values)).argsort()])
    
    return is_fair, ans_data    

def scores(anchors):
    scores_dic = {}
    counter = {}
    for anchor in anchors:
        for feature in anchor.exp_map['feature']:
            if feature not in scores_dic:
                scores_dic[feature] = 0
                counter[feature] = 1
            scores_dic[feature] += 2 * (anchor.precision() * anchor.coverage()) / (anchor.precision() + anchor.coverage())
            counter[feature] += 1
    
    for key in scores_dic.keys():
        scores_dic[key] = scores_dic[key] / counter[key]
    
    return scores_dic

def non_empty_anchors_fn(anchors):
    
    non_empty_anchors_list = []
    for anchor in anchors:
        if len(anchor.exp_map['feature']) > 0:
            non_empty_anchors_list.append(anchor)
    
    return non_empty_anchors_list

def distinct_anchors(anchors):
    
    distinct_anchor_set = set()
    distinct_anchor_list = []
    for anchor in anchors:
        key = ' '.join(anchor.names())
        if key not in distinct_anchor_set:
            distinct_anchor_set.add(key)
            distinct_anchor_list.append(anchor)
            
    return distinct_anchor_list

def pareto_set(anchors):
    
    anchors = np.array(anchors)
    pareto=oapackage.ParetoDoubleLong()

    for i in range(len(anchors)):
        anchor = anchors[i]
#         print(anchor.precision(), anchor.coverage())
        w=oapackage.doubleVector( (anchor.precision(), anchor.coverage()))
        pareto.addvalue(w, i)
    
    pareto.show(verbose=0)
    
    lst=list(pareto.allindices()) # the indices of the Pareto optimal designs
    
    optimal_anchors=anchors[lst]
      
    return optimal_anchors

def multi_pareto_set(anchors,n=1):
    
    current_anchors = anchors.copy()
    optimal_anchors = []
    
    for i in range(n):
    
        current_anchors = np.array(current_anchors)
        pareto=oapackage.ParetoDoubleLong()
    
        for i in range(len(current_anchors)):
            anchor = current_anchors[i]
    #         print(anchor.precision(), anchor.coverage())
            w=oapackage.doubleVector( (anchor.precision(), anchor.coverage()))
            pareto.addvalue(w, i)
        
        pareto.show(verbose=0)
        
        lst=list(pareto.allindices()) # the indices of the Pareto optimal designs
        
        optimal_anchors += current_anchors[lst].tolist()
        print("anchors:",len(current_anchors),"to be deleted:",len(current_anchors[lst]),end='')
        
        current_anchors = np.delete(current_anchors, lst)
        
        print("afetr:",len(current_anchors))
    
#         print(optimal_anchors)
         
#         x = [anchor.precision() for anchor in current_anchors]
#         y = [anchor.coverage() for anchor in current_anchors]
#          
#         optimal_x = [anchor.precision() for anchor in optimal_anchors]
#         optimal_y = [anchor.coverage() for anchor in optimal_anchors]
#           
#         h=plt.plot(x, y, '.b', markersize=16, label='Non Pareto-optimal')
#         hp=plt.plot(optimal_x, optimal_y, '.r', markersize=16, label='Pareto optimal')
#         plt.xlabel('precision', fontsize=16)
#         plt.ylabel('coverage', fontsize=16)
#         plt.xticks([])
#         plt.yticks([])
#         _=plt.legend(loc=3, numpoints=1)
#           
#         plt.show() 
    
    return optimal_anchors
    
