"""
Implements SHAP_Global.
"""
import pandas as pd
import numpy as np

clusters=10

import shap


def features_contributions(predict_fn, train, feature_names, max_features, class_names, categorical_features, categorical_names, sample_size, kernel_width=3):
    
    choosen = shap.sample(train, sample_size)
    sample = shap.kmeans(train, clusters)
    explainer = shap.KernelExplainer(predict_fn,sample)
    shap_values = explainer.shap_values(choosen)
    
    return explainer, shap_values

def fairness_eval(model, train, max_features, sensitive_features, feature_names, class_names, categorical_features, categorical_names, sample_size, threshold=None):
    
    explainer, shap_values = features_contributions(model.predict_proba, train, feature_names, max_features, class_names, categorical_features, categorical_names, sample_size)
    
    n_features = len(feature_names)
    
    contributions = {}  
    for i in range(n_features):
        contributions[i] = sum(shap_values[0][:,i])
             
    actual_sensitive, is_fair, df = fairness_valid_top(contributions, feature_names, sensitive_features, max_features)
     
    return actual_sensitive, is_fair, df, explainer

def fairness_valid_top(contributions, feature_names, sensitive_features, max_features):
    
    actual_sensitive = []
    counter_top = 0
    ans_data = []
    sorted_dict = sorted(contributions.items(), key=lambda x: abs(x[1]), reverse=True)
    
    for key,value in sorted_dict:
        ans_data1 = [key,feature_names[key],value]
        ans_data.append(ans_data1)
        
        if key in sensitive_features: 
            actual_sensitive.append(key)
        
        counter_top += 1
        if counter_top >= max_features:
            break
            
    
    df = pd.DataFrame(ans_data, columns = ["Index", "Feature", "Contribution"])
    return actual_sensitive, len(actual_sensitive) < 2, df

