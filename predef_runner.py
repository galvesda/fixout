import argparse
import datetime
import pickle

from runner import FixOut


def main(pickle_source, max_features, algo, exp, sampling_size):
       
    batchs,labels_test,class_names,to_drop,all_categorical_f,feature_names,categorical_names = pickle.load( open( pickle_source+".p", "rb" ) )
    
    limeout = FixOut(max_features, algo, exp, sampling_size)   
    
    is_fair, model , _ = limeout.is_fair()
         
    if not is_fair:
        is_fair, ensemble, _ = limeout.ensemble_out()
        return ensemble
     
    else:
        return model    
   
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='ExpOut: process fairness for classification')
    parser.add_argument('--pickle')  
    parser.add_argument('--algo')  
    parser.add_argument('--exp')  
    parser.add_argument('--samplesize', type=int)  
    parser.add_argument('--max_features', type=int)  

    args = parser.parse_args()
     
    now = datetime.datetime.now()
    print(now.year,'-', now.month,'-', now.day,',', now.hour,':', now.minute,':', now.second,sep='')
    
    main(args.pickle, args.max_features, args.algo, args.exp, args.samplesize)
